package event

import (
	"encoding/json"
	"time"

	"github.com/Shopify/sarama"
	"github.com/satori/go.uuid"
)

type Event struct {
	ID        string                 `json:"id"`
	UserID    string                 `json:"user_id"`
	Type      string                 `json:"type"`
	Action    string                 `json:"action"`
	Data      map[string]interface{} `json:"data"`
	CreatedAt time.Time              `json:"created_at"`
}

type Eventable interface {
	Send(Event) error
}

func NewEvent() Event {
	evt := Event{}
	uid, _ := uuid.NewV4()
	evt.ID = uid.String()
	evt.CreatedAt = time.Now()

	return evt
}

func (e Event) Send(event Event) error {
	config := sarama.NewConfig()
	config.Producer.Return.Successes = true
	producer, err := sarama.NewSyncProducer([]string{"localhost:9092"}, config)
	if err != nil {
		panic(err)
	}

	evt, err := json.Marshal(event)
	if err != nil {
		panic(err)
	}

	message := &sarama.ProducerMessage{
		Topic: "testing",
		Value: sarama.StringEncoder(string(evt)),
	}

	_, _, err = producer.SendMessage(message)
	if err != nil {
		panic(err)
	}

	return nil
}
