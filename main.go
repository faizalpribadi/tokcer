package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"

	"github.com/Shopify/sarama"
	"github.com/faizalpribadi/api/controllers"
	"github.com/faizalpribadi/api/db"
	"github.com/faizalpribadi/api/event"
	"github.com/faizalpribadi/api/socket/client"
	"github.com/faizalpribadi/api/socket/hub"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
)

var (
	hubhub = hub.NewHub()
)

func consumerListener() {
	log.Println("Worker Consumer is started and connected")

	uid, _ := uuid.NewV4()
	config := sarama.NewConfig()
	config.ClientID = "INFONESIA-WORKER-" + uid.String()
	config.Consumer.Return.Errors = true

	master, err := sarama.NewConsumer([]string{"localhost:9092"}, config)
	if err != nil {
		panic(err)
	}

	defer func() {
		if err := master.Close(); err != nil {
			panic(err)
		}
	}()

	consumer, err := master.ConsumePartition("testing", 0, sarama.OffsetNewest)
	if err != nil {
		panic(err)
	}

	messageCountStart := 0
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt)
	doneCh := make(chan struct{})

	go func() {
		for {
			select {
			case err := <-consumer.Errors():
				fmt.Println(err)

			case msg := <-consumer.Messages():
				messageCountStart++
				event := &event.Event{}
				err := json.Unmarshal(msg.Value, &event)
				if err != nil {
					panic(err)
				}
				if hubhub.HasClient(event.UserID) {
					c, _ := hubhub.GetClient(event.UserID)
					c.Send(event)
				} else {
					log.Println("Could not sent to websocket, because user_id not found")
				}
				fmt.Println("Received messages", string(msg.Key), string(msg.Value))
			case <-signals:
				fmt.Println("Interrupt is detected")
				doneCh <- struct{}{}
			}
		}
	}()
	<-doneCh
	fmt.Println("Processed", messageCountStart, "messages")
}

func main() {
	db.Open()
	var dir string
	go consumerListener()

	flag.StringVar(&dir, "dir", "./public", "the directory to serve files from. Defaults to the current dir")
	flag.Parse()
	r := mux.NewRouter()
	chClient := make(chan client.Client)

	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(dir))))
	r.HandleFunc("/api/notification", hubhub.Serve(chClient))
	r.HandleFunc("/message", controllers.PostMessage).Methods("POST")
	middleware := handlers.CORS(
		handlers.AllowCredentials(),
		handlers.AllowedMethods([]string{"GET"}),
		handlers.AllowedHeaders([]string{"*"}),
		handlers.AllowedOrigins([]string{"*"}),
	)

	if err := http.ListenAndServe(":3000", middleware(r)); err != nil {
		log.Fatal(err)
	} else {
		log.Printf("Websocket is serving on %s\n", ":3000")
	}
}
