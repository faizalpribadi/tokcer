package client

import (
	"fmt"
	"time"

	"github.com/gorilla/websocket"
)

var (
	ErrWriteTimeout = fmt.Errorf("WriteTimeout")
)

type Client interface {
	ID() string
	Ping()
	Destroy()
	Send(interface{}) error
}

type client struct {
	conn         *websocket.Conn
	id           string
	writeTimeout time.Duration
	closed       bool
	chClose      chan struct{}
}

func NewClient(conn *websocket.Conn, userId string) Client {
	c := &client{conn, userId, 100 * time.Millisecond, false, make(chan struct{})}
	go c.Ping()
	return c
}

func (c *client) ID() string {
	return c.id
}

func (c *client) Ping() {
	ticker := time.NewTicker(100 * time.Millisecond)
	defer ticker.Stop()
	for {
		select {
		case <-c.chClose:
			return
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(c.writeTimeout))
			c.conn.WriteMessage(websocket.PingMessage, nil)
		}
	}
}

func (c *client) Send(data interface{}) error {
	c.conn.SetWriteDeadline(time.Now().Add(c.writeTimeout))
	// err := c.conn.WriteJSON(v interface{})(websocket.TextMessage, data)
	err := c.conn.WriteJSON(data)
	return err
}

func (c *client) Destroy() {
	c.chClose <- struct{}{}
}
