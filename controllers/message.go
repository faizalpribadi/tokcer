package controllers

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/faizalpribadi/api/event"
	"github.com/faizalpribadi/api/middlewares"
)

func PostMessage(res http.ResponseWriter, req *http.Request) {
	user, err := middlewares.UserInfo(req)
	if err != nil {
		panic(err)
	}

	log.Println(user)

	var evt event.Event
	dec := json.NewDecoder(req.Body)
	if err := dec.Decode(&evt); err != nil {
		log.Println(err)
	}

	if evt.UserID == "" {
		http.Error(res, "invalid user", 404)
	}

	eventable := event.NewEvent()
	eventable.UserID = evt.UserID
	eventable.Type = "post.event"
	eventable.Action = evt.Action

	if evt.Action == "" {
		http.Error(res, "invalid action", 500)
	}

	message := ""

	switch evt.Action {
	case "follow":
		message = "someone following you"
	case "question":
		message = "someone asking you"
	case "like":
		message = "someone like your post"
	case "comment":
		message = "someone commented on your post"
	}

	data := map[string]interface{}{
		"notification": message,
	}

	eventable.Data = data
	if err := eventable.Send(eventable); err != nil {
		log.Panic(err)
	}

	res.WriteHeader(200)
	_, _ = res.Write([]byte("OK"))
}
