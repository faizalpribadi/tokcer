package middlewares

import (
	"net/http"

	"github.com/faizalpribadi/api/db"
)

type User struct {
	UserID   string `json:"user_id"`
	Username string `json:"username"`
}

func UserInfo(req *http.Request) ([]User, error) {
	var user []User
	headerField := req.Header.Get("x-user")
	if err := db.Mysql.Select(&user, "SELECT * FROM users WHERE username = ?", headerField); err != nil {
		panic(err)
	}

	return user, nil
}
