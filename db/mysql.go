package db

import (
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

var Mysql *sqlx.DB

func Open() {
	var err error
	Mysql, err = sqlx.Open("mysql", "root:yasmin192@tcp(127.0.0.1:3306)/notification_testing")
	if err != nil {
		panic(err)
	}

	if err := Mysql.Ping(); err != nil {
		panic(err)
	}

	log.Println("connected to database ..")
}
