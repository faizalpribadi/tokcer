-- - Create Notification Schema

-- - Create User Profile

-- - Create Relationship ( user_id & with_user_id & status )

create table notifications (
    id varchar(55) NOT NULL,
    type varchar(55) NOT NULL,
    message varchar(250) NOT NULL,
    user_id varchar(55) NOT NULL
)

create table users (
    user_id varchar(55) NOT NULL,
    username varchar(55) NOT NULL,
)

create table relationship (
    user_id varchar(55) NOT NULL,
    with_user_id varchar(55) NOT NULL,
    status ENUM("follows", "block")
)
